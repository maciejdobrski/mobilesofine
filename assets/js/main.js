$(function(){
    var shrinkHeader = 490;

    var scroll = getCurrentScroll();
    if ( scroll >= shrinkHeader ) {
        $('.top').addClass('shrink');
    }
    else {
        $('.top').removeClass('shrink');
    }

    $(window).scroll(function() {
        var scroll = getCurrentScroll();
        if ( scroll >= shrinkHeader ) {
            $('.top').addClass('shrink');
        }
        else {
            $('.top').removeClass('shrink');
        }
    });


    function getCurrentScroll() {
        return window.pageYOffset || document.documentElement.scrollTop;
    }
    $('.navbar-toggle').click(function(){
        $(this).toggleClass('open');
    });

    $('#open-hmb-menu').click(function(){
        $('#hmb-menu').toggleClass('open');
    });

    $('#hmb-menu a').click(function(){
        $('#hmb-menu').toggleClass('open');
        $('.navbar-toggle').toggleClass('open');
    });

    $("#carousel-example-generic").swipeleft(function() {
        $(this).carousel('next');
    });
    $("#carousel-example-generic").swiperight(function() {
        $(this).carousel('prev');
    });
    $('.carousel').carousel({
        interval: 0
    });
});